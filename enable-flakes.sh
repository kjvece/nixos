#!/usr/bin/env sh

mkdir -p ~/.config/nix
NIX_CONF="$HOME/.config/nix/nix.conf"
touch $NIX_CONF
if [ -z "$(grep 'experimental-features' $NIX_CONF)" ]; then
  echo "experimental-features = nix-command flakes" >> $NIX_CONF
fi
