{ nixpkgs, flake-utils, inputs, ... }:

flake-utils.lib.eachDefaultSystem (system:
  let
    pkgs = nixpkgs.legacyPackages.${system};
  in
  {
    packages.catppuccin-tmux = pkgs.stdenv.mkDerivation {
      name = "catppuccin-tmux";
      src = inputs.catppuccin-tmux;
      installPhase = ''
        mkdir $out
        cp -r ./* $out
        sed -i 's/set status-right "$loaded_modules_right"/set status-right "$loaded_modules_right "/' $out/catppuccin.tmux
      '';
    };
  }
)
