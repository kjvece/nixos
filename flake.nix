{
  description = "NixOS Configuration";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs?ref=nixpkgs-unstable";
    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nixvim = {
      url = "github:nix-community/nixvim";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nixos-wsl = {
      url = "github:nix-community/NixOS-WSL";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    flake-utils = {
      url = "github:numtide/flake-utils";
    };
    catppuccin-tmux = {
      url = "github:catppuccin/tmux";
      flake = false;
    };
  };

  outputs = { self, nixpkgs, home-manager, nixvim, nixos-wsl, flake-utils, catppuccin-tmux, ... }@inputs:
    let
      system = "x86_64-linux";
      #system = "aarch64-linux";
      #system = "aarch64-darwin";
      pkgs = nixpkgs.legacyPackages.${system};
      user = "user";
      installPyenv = false;
      packages = (import ./packages { inherit nixpkgs flake-utils inputs; });
      specialArgs = {
        inherit user nixvim system catppuccin-tmux installPyenv;
        packages = packages.packages;
      };
    in
    {
      inherit (packages) packages;

      nixosConfigurations.nixos = nixpkgs.lib.nixosSystem {
        inherit system;
        specialArgs = { inherit nixos-wsl user; };
        modules = [
          #./configs/vm
          ./configs/wsl
          home-manager.nixosModules.home-manager {
            home-manager = {
              useGlobalPkgs = true;
              useUserPackages = true;
              users.${user} = import ./hmconf/home.nix;
              extraSpecialArgs = specialArgs;
            };
          }
        ];
      };
      homeConfigurations = {
        ${user} = home-manager.lib.homeManagerConfiguration {
          inherit pkgs;
          modules = [
            ./hmconf/home.nix
          ];
          extraSpecialArgs = specialArgs;
        };
      };
    };
}
