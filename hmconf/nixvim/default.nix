{ pkgs, lib, homeDirectory, isMac, ... }:

let
  undoDir = ".neovim/undo";
in
{

  # install nerdfonts for nvim-tree
  fonts.fontconfig.enable = !isMac;
  home.packages = if (!isMac)
    then [
      (pkgs.nerdfonts.override {
        fonts = [ "FiraCode" "DroidSansMono" ];
      })
    ]
    else [
    ]
  ;
  home.sessionVariables.EDITOR = "nvim";

  # create a directory to store undo files
  home.file.${undoDir} = {
    enable = true;
    source = ./undo;
    recursive = true;
  };

  programs.nixvim = {

    enable = true;
    defaultEditor = true;
    viAlias = true;
    vimAlias = true;

    extraPackages = with pkgs; [
      fzf
      ripgrep
      rustfmt
      go_1_22
    ];

    globals = {
      mapleader = " ";
    };
    keymaps = [
      {
        key = "<leader>e";
        action = "<cmd>NvimTreeToggle<CR>";
        mode = [ "n" ];
      }
      {
        key = "<leader>nh";
        action = "<cmd>nohlsearch<CR>";
        mode = [ "n" ];
      }
      {
        key = "<C-h>";
        action = "<cmd>C-U>TmuxNavigateLeft<CR>";
        mode = [ "n" ];
      }
      {
        key = "<C-j>";
        action = "<cmd>C-U>TmuxNavigateDown<CR>";
        mode = [ "n" ];
      }
      {
        key = "<C-k>";
        action = "<cmd>C-U>TmuxNavigateUp<CR>";
        mode = [ "n" ];
      }
      {
        key = "<C-l>";
        action = "<cmd>C-U>TmuxNavigateRight<CR>";
        mode = [ "n" ];
      }
    ];

    colorschemes.catppuccin = {
      enable = true;
      settings = {
        flavour = "mocha";
        integrations = {
          NormalNvim = true;
          cmp = true;
          gitsigns = true;
          lsp_saga = true;
          nvimtree = true;
          treesitter = true;
        };
      };
    };

    opts = {
      # numbering
      number = true;
      relativenumber = true;

      # case sensitivity
      ignorecase = true;
      smartcase = true;

      # tabs
      expandtab = true;
      shiftwidth = 2;
      tabstop = 2;
      softtabstop = 2;
      smartindent = true;

      # show whitespace
      list = true;
      listchars = "tab:»»,trail:·";

      # buffer options
      hidden = true;
      updatetime = 200;
      splitright = true;
      splitbelow = true;

      # files
      swapfile = false;
      undodir = "${homeDirectory}/${undoDir}//";
      undofile = true;

      # display options
      background = "dark";
      signcolumn = "yes";
      termguicolors = lib.mkForce (!isMac);

      # which-key options
      timeout = true;
      timeoutlen = 300;
    };

    # don't do buffer numbering in terminal
    autoGroups = {
      Terminal.clear = true;
    };
    autoCmd = [
      {
        group = "Terminal";
        event = [ "TermOpen" ];
        command = "set nonumber norelativenumber";
      }
    ];

    # allow keywords to have '-' in them (useful for js classes)
    extraConfigLua = ''
      vim.opt.iskeyword:append("-")
    '';

    plugins = {
      lualine = {
        enable = true;
        componentSeparators = {
          left = "|";
          right = "|";
        };
        sectionSeparators = {
          left = " ";
          right = " ";
        };
      };
      gitsigns.enable = true;
      lsp = {
        enable = true;
        keymaps = {
          silent = true;
          lspBuf = {
            gD = "references";
            gd = "definition";
            gi = "implementation";
            gt = "type_definition";
          };
        };
        onAttach = ''
          local opts = { noremap = true; silent = true; buffer = bufnr }
          local keymap = vim.keymap
          keymap.set("n", "K", "<cmd>Lspsaga hover_doc<CR>", opts)
          keymap.set("n", "gf", "<cmd>Lspsaga finder<CR>", opts)
          keymap.set("n", "<leader>la", "<cmd>Lspsaga code_action<CR>", opts)
          keymap.set("n", "<leader>lr", "<cmd>Lspsaga rename<CR>", opts)
          keymap.set("n", "<leader>ld", "<cmd>Lspsaga show_line_diagnostics<CR>", opts)
          keymap.set("n", "<leader>ld", "<cmd>Lspsaga show_cursor_diagnostics<CR>", opts)
        '';
        servers = let
          changeTabWidth = n: ''
            vim.opt_local.shiftwidth = ${builtins.toString n}
            vim.opt_local.tabstop = ${builtins.toString n}
            vim.opt_local.softtabstop = ${builtins.toString n}
          '';
        in {
          lua-ls.enable = true;
          rust-analyzer = {
            enable = true;
            installRustc = true;
            installCargo = true;
          };
          tsserver.enable = true;
          jsonls.enable = true;
          clangd = {
            enable = true;
            onAttach.function = changeTabWidth 4;
          };
          cmake.enable = true;
          dockerls.enable = true;
          gopls = {
            enable = true;
            onAttach.function = ''
              vim.opt_local.expandtab = false;
            '';
            extraOptions = {
              settings = {
                gopls = {
                  usePlaceholders = true;
                  gofumpt = true;
                  staticcheck = true;
                  analyses = {
                    unusedparams = true;
                    unusedvariable = true;
                    nilness = true;
                    defers = true;
                    assign = true;
                    appends = true;
                    copylocks = true;
                    fieldalignment = true;
                    simplifyrange = true;
                  };
                  hints = {
                    assignVariableTypes = true;
                    compositeLiteralFields = true;
                    rangeVariableTypes = true;
                  };
                };
              };
            };
          };
          nixd.enable = true;
          tailwindcss.enable = true;
          terraformls.enable = true;
          yamlls.enable = true;
          "java-language-server".enable = true;
          pylsp = {
            enable = true;
            onAttach.function = changeTabWidth 4;
          };
          pyright.enable = true;
        };
      };
      lspsaga = {
        enable = true;
      };
      lsp-format = {
        enable = true;
        lspServersToEnable = "all";
      };
      cmp-nvim-lsp.enable = true;
      cmp-buffer.enable = true;
      cmp-path.enable = true;
      cmp_luasnip.enable = true;
      luasnip = {
        enable = true;
        fromVscode = [ {} ];
      };
      "friendly-snippets".enable = true;
      cmp = {
        enable = true;
        autoEnableSources = true;
        settings = {
          autocomplete = [
            "require('cmp.types').cmp.TriggerEvent.TextChanged"
          ];
          completeopt = "menu,menuone,noselect";
          keyword_length = 1;
          preselect = "cmp.PreselectMode.None";
          mapping = {
            "<C-e>" = "cmp.mapping.abort()";
            "<C-n>" = "cmp.mapping.select_next_item()";
            "<C-p>" = "cmp.mapping.select_prev_item()";
            "<CR>" = ''
              cmp.mapping(function(fallback)
                local luasnip = require('luasnip')
                if cmp.visible() and cmp.get_active_entry() then
                  if luasnip.expandable() then
                    luasnip.expand()
                  else
                    cmp.confirm({
                      select = true,
                    })
                  end
                else
                  fallback()
                end
              end, { "i", "s" })
            '';
            "<Tab>" = ''
              cmp.mapping(function(fallback)
                local luasnip = require('luasnip')
                if cmp.visible() then
                  cmp.select_next_item()
                elseif luasnip.locally_jumpable(1) then
                  luasnip.jump(1)
                else
                  fallback()
                end
              end, { "i", "s" })
            '';
            "<S-Tab>" = ''
              cmp.mapping(function(fallback)
                local luasnip = require('luasnip')
                if cmp.visible() then
                  cmp.select_prev_item()
                elseif luasnip.jumpable(-1) then
                  luasnip.jump(-1)
                else
                  fallback()
                end
              end, { "i", "s" })
            '';
          };
          snippet.expand = ''
            function(args)
              require('luasnip').lsp_expand(args.body)
            end
          '';
          sources = [
            { name = "nvim_lsp"; }
            { name = "luasnip"; }
            { name = "path"; }
            { name = "buffer"; }
          ];
        };
      };
      treesitter.enable = true;
      telescope = {
        enable = true;
        keymaps = {
          "<C-p>" = "find_files";
          "<leader>fg" = "live_grep";
          "<leader>ff" = "find_files";
          "<leader>fv" = "git_files";
          "<leader>fs" = "lsp_document_symbols";
        };
      };
      nvim-tree = {
        enable = true;
        autoClose = true;
      };
      tmux-navigator.enable = true;
      "which-key" = {
        enable = true;
        ignoreMissing = true;
        registrations = {
          "<leader>e" = "toggle file [e]xplorer";
          "<leader>f" = "[f]ind files/content";
          "<leader>fg" = "find via [g]rep";
          "<leader>fv" = "find within [v]cs (e.g. git)";
          "<leader>ff" = "find [f]iles (also Ctrl+P)";
          "<leader>fs" = "find [s]ymbols";
          "<leader>l" = "[l]sp commands (language support)";
          "<leader>la" = "code [a]ction";
          "<leader>lr" = "[r]ename";
          "<leader>ld" = "[d]iagnostics";
        };
      };
    };

  };

}
