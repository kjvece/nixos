{ pkgs, ... }:
let
  initExtra = ''
    export PKG_CONFIG_PATH=$(home-manager generations | head -n1 | awk '{ print $7 }')/home-path/lib/pkgconfig
  '';
in
{
  programs.pyenv.enable = true;

  programs.bash.initExtra = initExtra;
  programs.zsh.initExtra = initExtra;

  home.packages = with pkgs; [
    home-manager
    gcc
    gnumake
    pkg-config
    zlib
    bzip2
    readline
    sqlite
    openssl
    tk
    libffi
    xz
    ncurses
    zlib.dev
    bzip2.dev
    readline.dev
    sqlite.dev
    openssl.dev
    tk.dev
    libffi.dev
    xz.dev
    ncurses.dev
  ];
}
