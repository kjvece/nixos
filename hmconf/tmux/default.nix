{ pkgs, lib, packages, system, ... }:

{
  programs.tmux = {
    enable = true;
    shell = "${pkgs.zsh}/bin/zsh";
    shortcut = "s";
    baseIndex = 1;
    clock24 = true;
    escapeTime = 0;
    keyMode = "vi";
    historyLimit = 100000;
    disableConfirmationPrompt = true;
    mouse = true;
    terminal = "screen-256color";
    extraConfig = lib.strings.concatStringsSep "\n" [
      (import ./display.nix)
      (import ./keybindings.nix)
      "run-shell ${packages.${system}.catppuccin-tmux}/catppuccin.tmux"
      "run-shell ${pkgs.tmuxPlugins.cpu}/share/tmux-plugins/cpu/cpu.tmux"
    ];
    plugins = with pkgs.tmuxPlugins; [
      yank
    ];
  };
}
