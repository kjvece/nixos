''
bind -n C-left resize-pane -L 1
bind -n C-right resize-pane -R 1
bind -n C-down resize-pane -D 1
bind -n C-up resize-pane -U 1

bind s split-window -v -c "#{pane_current_path}"
bind v split-window -h -c "#{pane_current_path}"

bind w choose-window
bind S choose-session
bind c kill-pane
bind N new-window

bind-key -T copy-mode-vi v send-keys -X begin-selection

is_vim="ps -o state= -o comm= -t '#{pane_tty}' \
    | grep -iqE '^[^TXZ ]+ +(\\S+\\/)?g?(view|l?n?vim?x?|fzf)(diff)?$'"
bind-key 'h' if-shell "$is_vim" 'send-keys C-h'  'select-pane -L'
bind-key 'j' if-shell "$is_vim" 'send-keys C-j'  'select-pane -D'
bind-key 'k' if-shell "$is_vim" 'send-keys C-k'  'select-pane -U'
bind-key 'l' if-shell "$is_vim" 'send-keys C-l'  'select-pane -R'

bind-key -Tcopy-mode-ctrl-w h select-pane -L
bind-key -Tcopy-mode-ctrl-w j select-pane -D
bind-key -Tcopy-mode-ctrl-w k select-pane -U
bind-key -Tcopy-mode-ctrl-w l select-pane -R

bind-key -T copy-mode-vi 'C-w' switch-client -Tcopy-mode-ctrl-w
''
