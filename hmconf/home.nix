{ config, pkgs, lib, user, nixvim, system, packages,... }@args:

let
  isMac = builtins.match ".+-darwin$" system != null;
  homeDirectory = if (isMac)
    then "/Users/${user}"
    else if (user == "root")
      then "/root"
      else "/home/${user}"
  ;
  profilePath = "${homeDirectory}/.nix-profile/etc/profile.d";
  execProfile = fileName: ''
    if [ -f "${profilePath}/${fileName}" ]; then
      . "${profilePath}/${fileName}"
    fi
  '';
  dotProfile = ''
    ${execProfile "nix.sh"}
    ${execProfile "nix-daemon.sh"}
    ${execProfile "hm-session-vars.sh"}
  '';
  inputs = {
    inherit pkgs lib packages system homeDirectory isMac;
  };
in
{
  imports = [
    nixvim.homeManagerModules.nixvim
    (import ./nixvim inputs)
    (import ./tmux inputs)
  ] ++ (
    if args.installPyenv
    then [ (import ./pyenv inputs) ]
    else []
  )
  ;

  nixpkgs = {
    config = {
      allowUnfree = true;
      allowUnfreePredicate = (_: true);
    };
  };

  home.username = user;
  home.homeDirectory = homeDirectory;

  home.stateVersion = "23.11";

  home.packages = with pkgs; [
    curl
  ];

  home.shellAliases = {
    "init-project" = "nix flake init --template templates#utils-generic";
    "new-project" = "nix flake new --template templates#utils-generic";
    "ls" = "ls --color=auto -F";
  };

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;

  xdg.configFile."starship.toml".source = ./starship.toml;

  programs = {
    bash = {
      enable = true;
      initExtra = dotProfile;
    };
    zsh = {
      enable = true;
      initExtra = dotProfile;
      autosuggestion.enable = true;
      defaultKeymap = "emacs";
    };
    starship.enable = true;
    direnv.enable = true;
    git = {
      enable = true;
      lfs.enable = true;
      userName = "user";
      userEmail = "email@example.com";
      ignores = [
        "*.swp"
        "*.pyc"
        "build/*"
        "target/*"
        "env/*"
        "venv/*"
        ".env"
      ];
      extraConfig = {
        init.defaultBranch = "main";
        pull.rebase = true;
        push.autoSetupRemote = true;
      };
    };
  };
}
